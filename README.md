# QWorld Documentation repository

In this repository, we have the original form of the QWorld documentation, which includes the Governance of QWorld, Code of Conducts and Code of Ethics of QWorld and its channel.

Any document becomes official after being available on release branch. Any other branch including master is used for development or some other purposed, and any part of them **cannot be official** until appearing on release branch.

Currently, no one (including maintainers) is allowed to push anything to release branch. If anyone would like to modify the release branch, the merge request is required.

In case of technical issues, please contact the coordinator of QKitchen (qkitchen [at] qworld.lu.lv).
