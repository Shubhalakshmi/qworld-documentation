# QSisters

In this document, we list QSisters in the order of joining the network (entanglement).

http://qworld.lu.lv/index.php/qwomen/

## QWomen@Istanbul

http://qworld.lu.lv/index.php/qwomenistanbul/

qwomen-istanbul [at] qworld.lu.lv

Founded on December 2019.

## QWomen@Ankara

http://qworld.lu.lv/index.php/qwomenankara/

qwomen-ankara [at] qworld.lu.lv

Founded on December 2019.

