# Code of Conduct for QWorld's QKitchen workspace

Version: *30.03.2020*

QWorld's GitLab workspace *QKitchen* is a channel that allows collaboratively preparing and developing educational materials, software, and other documents and then sharing them with the public. Anyone interested in preparing quantum related educational materials, software, or other documents are encouraged to join us in this endeavour. 

Any contribution, public or private, not related to the topic may be removed, including files, tags, comments, etc.

## Purpose

A primary goal of QWorld is to be inclusive to the largest number of contributors, with the most varied and diverse backgrounds possible. We are committed to providing a friendly, safe and welcoming environment for everyone, regardless of gender, sexual orientation, ability, ethnicity, socioeconomic status, and religion or lack thereof.

This Code of Conduct outlines our expectations for all those who participate in our QKitchen community, as well as the consequences for unacceptable behaviour. This isn't an exhaustive list of things that you can't do. Rather, take it in the spirit in which it's intended - a guide to make it easier to enrich all of us and the technical communities in which we participate.

We invite all those who participate in QWorld activities to help us create a safe and positive experience for everyone.

## Creating a Welcoming and Inclusive Culture

Communities mirror the societies in which they exist and positive action is essential to counteract the many forms of inequality and abuses of power that exist in society.

If you see someone who is making an extra effort to ensure our community is welcoming, friendly, and encourages all participants to contribute to the fullest extent, please recognize their efforts.

## Contribution

Members are allowed to ask the coordinator(s) of QKitchen to provide a workspace with suitable writing permissions so that they can develop their content. The workspace may take the form of a repository, or branch. The form and location of the workspace are set with an agreement of the coordinator(s) of QKitchen.

Any participant not being a member of QWorld is encouraged to contribute by sending a pull request. Any active participant of QKitchen can apply for being a member of QWorld.

QKitchen is a public repository, so that anyone interested in our activities may see its content, which includes contributors' personal data. A project can be developed privately up to a moment on QKitchen, but it should be public at the end. At the same time, we require the main contributors of the content to provide their name and email address for contact. These should be included in a visible place in the project, for example, in a README file. In case of contributors providing small corrections, for example, typos, their name or nick should be placed in a public, visible place of the projects. Contributors providing small corrections may provide their contact addresses as well. Contributors providing small corrections may refuse to acknowledge them in the project or to remove their personal data from the final version of the project. 

Impersonating other people while contributing is strictly prohibited and is a serious violation of this Code of Conduct.

## Ownership and license

Any content that appears on a QKitchen workspace belongs to its author(s) or the company/organization they represent. QKitchen is allowed to store, modify, transfer, remove, and utilize the content accordingly. This includes any contributor who is willing to continue and improve the original content.

QKitchen uses open-source licenses. The default licences of QKitchen are as follows: 

* CC-BY-4.0 (or earlier versions) for the content, documentation, texts, graphics, and similar materials, available at [https://creativecommons.org/licenses/by/4.0/legalcode](https://creativecommons.org/licenses/by/4.0/legalcode)

* Apache License 2.0 for code and software, available at [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Contributors are obliged to licence their work with the proper note. Contributors, with the consent of coordinator(s) of the QKitchen, and possibly the consent of the Board of QWorld (QBoard) may publish the code under a different open-source license. QBoard's decision is superior over the decision of the coordinator(s) of the QKitchen.

## Expected Behavior

The following behavior is expected and requested of all community members:

* Be friendly and patient.

* Be welcoming. We strive to be a community that welcomes and supports people of all backgrounds and identities. This includes, but is not limited to members of any race, ethnicity, culture, national origin, color, immigration status, social and economic class, educational level, sex, sexual orientation, gender identity and expression, age, size, family status, political belief, religion, and mental and physical ability.

* Be considerate. Your work can be used by other people, and you, in turn, will depend on the work of others. Any decision you take will affect users and colleagues, and you should take those consequences into account when making decisions. Remember that we're a world-wide community, so you might not be communicating in someone else's primary language.

* Be respectful. Not all of us will agree all the time, but disagreement is no excuse for poor behavior and poor manners. We might all experience some frustration now and then, but we cannot allow that frustration to turn into a personal attack. It's important to remember that a community where people feel uncomfortable or threatened is not a productive one. Members of the QWorld community should be respectful when dealing with other members as well as with people outside the QWorld community.

* Be careful in the words that you choose. We are a community of professionals, and we conduct ourselves professionally. Be kind to others. Do not insult or put down other participants. Harassment and other exclusionary behavior aren't acceptable. This includes, but is not limited to:

    * violent threats or language directed against another person;

    * discriminatory jokes and language;

    * posting sexually explicit or violent material;

    * posting (or threatening to post) other people's personally identifiable information ("doxing");

    * personal insults, especially those using racist or sexist terms;

    * unwelcome sexual attention;

    * advocating for, or encouraging, any of the above behaviors;

    * repeated harassment of others (in general, if someone asks you to stop, then stop).

* When we disagree, try to understand the reason. Disagreements, both social and technical, happen all the time, and QWorld is no exception. It is important that we resolve disagreements and differing views constructively. Remember that we're different. The strength of QWorld comes from its varied community, people from a wide range of backgrounds. Different people have different perspectives on issues. Being unable to understand why someone holds a viewpoint doesn't mean that they're wrong. Don't forget that it is human to err and blaming each other doesn't get us anywhere. Instead, focus on helping to resolve issues and learning from mistakes.

* Maintarners and Coordinators reserve the right to delete excessive self-promotional or commercial materials.

* Content that has been identified as objectionable, inappropriate, or off-topic will be subject to deletion by channel moderators. Posters will receive a warning and risk being blocked from the channel if unacceptable behavior persists.

These decisions are made by QWorld at its sole discretion.

## Consequences of Unacceptable Behavior

Unacceptable behavior from any community member, including staff and those with decision-making authority, will not be tolerated.

Anyone asked to stop unacceptable behavior is expected to comply immediately.

If a community member engages in unacceptable behavior, coordinator(s) may take any action deemed appropriate, up to and including a temporary ban or permanent expulsion from the community without warning.

## Reporting Guidelines

If you are subject to or witness unacceptable behavior, or have any other concerns, please notify the coordinator(s) of QKitchen as soon as possible by email (qkitchen \[at\] qworld.lu.lv). You may also directly contact the Ethics Committee of QWorld ([http://qworld.lu.lv/index.php/contact/](http://qworld.lu.lv/index.php/contact/)).

All complaints will be investigated and appropriate steps will be taken to keep the GitLab community safe and friendly, which may include contacting the person responsible for improper content if necessary. All coordinators and others involved will maintain confidentiality with regard to the reporter of an incident.

To discuss a decision, contact the coordinator(s) or the members of the Ethics Committee of QWorld.

## Incident Response

If the coordinator agrees that the content violates the Code of Conduct, the content is modified with the permission of the author. If the author does not give permission to modify the content accordingly, the content will be removed from the GitLab repository, if possible. In the case of repeated or serious breaking of Code of Conduct, the coordinators may revoke the permission of the author to modify the GitLab content, if given previously, and immediately reject future pull requests. Please be aware that violating the QWorld GitLab Code of Conduct implies violating the QWorld Code of Conduct, hence additional steps may be taken.

## Modification 

QWorld may modify this Code of Conduct, in which case the coordinators of QKitchen are responsible for informing all members of QWorld about this by email, providing the link to the modified file. Current and past versions of the Code of Conduct are available on the official website of QWorld, so all contributors may follow modifications. 

## License and attribution

This code of conduct was directly adapted from Slack/Code of Conduct of Creative Commons [https://wiki.creativecommons.org/wiki/Slack/Code_of_Conduct](https://wiki.creativecommons.org/wiki/Slack/Code_of_Conduct), which was directly adopted from the Stumptown Syndicate ([http://stumptownsyndicate.org/code-of-conduct/](http://stumptownsyndicate.org/code-of-conduct/)) and distributed under a Creative Commons Attribution-ShareAlike license. It was also noted that "Major portions of text derived from the Django Code of Conduct and the Geek Feminism Anti-Harassment Policy".

