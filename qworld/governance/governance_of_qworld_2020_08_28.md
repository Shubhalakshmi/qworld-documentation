# Governance of QWorld

Version: *August 29, 2020*

### Who we are

QWorld is a global network of individuals, groups, and communities collaborating on education and implementation of quantum technologies and research activities.

### What we do

QWorld works to popularize quantum technologies and quantum software and to involve more people to the field by working publicly and/or academically and locally and/or internationally.

### Our Goal

Having an open access and public global ecosystem for quantum technologies and quantum software by the year 2025 so that each interested hardworking individual, group, institute, or region can be easily part of the ecosystem.

## Short history

The ideas of [*QWorld*](http://qworld.lu.lv/index.php/qworld/) and [*QCousin*](http://qworld.lu.lv/index.php/qcousins/) was first formulated by Abuzer Yakaryilmaz before starting to implement [QDrive project](https://qsoftware.lu.lv/index.php/qdrive/) (May-July 2019), and both ideas have been developed during QDrive project.

[QTurkey](http://qworld.lu.lv/index.php/qturkey/) was the first group accepting to be a QCousin (Zeki Seskir, Ozlem Salehi Koken, and Hande Alemdar) of [QLatvia](http://qworld.lu.lv/index.php/qlatvia/) in May 2019.

[QHungary](http://qworld.lu.lv/index.php/qhungary/) was the second QCousin (beginning of July 2019), and the ideas of _QWorld_ and _QCousin_ took their first written form with the support and contribution of the members of QHungary (Zoltán Zimborás, Janos Asboth, Andras Palyi, and László Oroszlány), Andris Ambainis, and QDrive team members Maksims Dimitrijevs, Agnieszka Wolska, and Aleksejs Naumovs.

By the end of QDrive project, [QBalkan](http://qworld.lu.lv/index.php/qbalkan/) (Mohamed Elsayed Yahia) and [QPoland](http://qworld.lu.lv/index.php/qpoland/) (Paweł Gora, Jarosław Miszczak, Adam Glos, and Oskar Słowik) also joined QCousin network. At the same time, the first global project of QCousins "_[the Entangling QCousins](http://qworld.lu.lv/index.php/entangling-qcousins/)_" was designed and _[QKitchen](http://qworld.lu.lv/index.php/qkitchen/)_ was established under QWorld.

 Under the leadership of Abuzer Yakaryilmaz, QWorld started its _initialization phase_ with the additional channels [*QWomen*](http://qworld.lu.lv/index.php/qwomen/), [*QJunior*](http://qworld.lu.lv/index.php/qjunior/), [*QUniversity*](http://qworld.lu.lv/index.php/quniversity/), and [*QMentor Training*](http://qworld.lu.lv/index.php/qmentor-training/) at the end of October 2019\. As a part of the process, *the code of ethics*, *the code of conduct*, and *certain regulations* were scheduled to be defined, [*the Ethics Committee of QWorld*](http://qworld.lu.lv/index.php/contact/) was formed, and [the website of QWorld](http://qworld.lu.lv/) was designed by *QDesigner* Agnieszka Wolska. As a nice addition, on the same days, QWorld received [its first grant](http://qworld.lu.lv/index.php/unitary-fund/) from _Unitary Fund_ for supporting some local workshops of QCousins (thanks to *Will Zeng*).

## Voting

There are three types of votes: positive, negative, or neutral. If a voter does not vote, then it is counted as neutral for simplicity.

Whenever it is needed, a voting committee is formed by at least two members of the Board of QWorld.

The voting committee is responsible for every voting related process. Except the determined/defined rules and regulations, the voting committee determines, defines, and follows the voting procedures. The procedure should be open and public to the voters and candidates, unless it is determined otherwise before forming the voting committee.

Voting process is processed electronically (e.g., emails, doodle, etc.), unless specified otherwise.

## Membership

A person operating in the team of an event or a project or a channel of QWorld becomes ***a member of QWorld***.

During the initialization phase of QWorld or a channel of QWorld, a corresponding member can take the title of ***integration member*** up to one year in order to take part in the governance of QWorld whenever it is applicable. This title is finalized once the initialization phase is ended, and so any such member is expected to work towards getting the title of leading member.

A member who has at least six month experience in QWorld and successfully completed one of the following (or similar) tasks can apply for getting the title of ***leading member***:

* one big scale duty, e.g., successfully performing one of the main roles of a project or an event taking more than two weeks, or, leading a project or an event taking more than five days, or, preparing an educational material that can be used for a two-day long workshop, and then leading such workshop;
* two medium scale duties, e.g., leading a project or an event, and, successfully performing one of the main roles of an event or a project;
* six small scale duties, e.g., a small scale duty can be being a "good" QMentor during a project or an event, or, actively performing a task of an event or a project, or, being a contributor in an educational material preparing project; or,
* one medium and three small scale duties.

In order to be selected as a leading member, the candidate member should prepare a list of his or her completed or ongoing duties and tasks. The board of QWorld processes the application. The assessment of performance of the candidate for each duty may be asked from the leader of the project/event or the coordinator of the channel. The board of QWorld is asked for voting:

* The title is given to the candidate if at least 80% of the votes are positive.
* The application is returned back to the candidate if at most 50% of the votes are positive or if at least 25% of the votes are negative.
* In any other case: The board of QWorld may call all leading members and integration members of QWorld for the same voting. The title is given to the candidate if at least 66% of the votes are positive. Otherwise, the application is returned back to the candidate.

The title of leading member is given for a year. A leading member can apply again for the title of leading member. In the new application, the previous duties cannot be used. A leading member can work in one or more channels of QWorld. If a leading member has been working in a channel for more than 3 months as a member of the channel and successfully completes one medium or three small scale duties for this channel, then he or she is also accepted as **a leading member of this channel**.

## Titles and responsibilities

There are two types of titles: *coordinators* and *technical titles*.

A technical title is given to a member (preferable leading member if the technical task is not so specific) who is responsible for one or a few technical issues, e.g., taking care of a (some) communication channel(s) (e-mail group, slack, etc), taking care of a (some) social media channel(s) (Facebook page, Twitter account, etc.), etc.

A coordinator title gives a central position with many responsibilities such as operating a channel, operating budget, or operating a big project (QDrive, QSemester, etc.). A bunch of technical issues can also be collected under a coordination title such as being a coordinator for public relations.

## The Board of QWorld

The board of QWorld is composed by the coordinators of QWorlds and its channels. Exceptionally, some leading members can also be selected to the board depending on the needs of QWorld.

## The initialization phase of QWorld

The initialization phase of QWorld starts on November 11, 2019. Once the number of leading members reaches fifteen (15), the Board of QWorld has the right to organize a voting to finalize the initialization phase: (i) the voting is organized among the leading members; (ii) the initialization phase ends if the number of positive votes is more than 66%; and (iii) the next voting cannot be earlier than two months.

Once the phase ends, each "integration member" title, given due to the initialization phase of QWorld, is finalized and the new coordinators of QWorld are elected by voting among the leading members. Each candidate must be a leading member. The exception can be made only for the first election when there is not enough candidates, and each member of QWorld having at least six month experience can be a candidate in this case.

The current coordinators of QWorld are listed on [the contact page of QWorld](http://qworld.lu.lv/index.php/contact/).

## The QWorld’s channels

With the initialization of QWorld, the following channels are defined. Each channel is also in its initial phase. Once a channel has 7 leading members, its coordinators have the right to organize a voting to finalize the initialization phase: (i) the voting is organized among the leading members of the channel; (ii) the initialization phase ends if the number of positive votes is more than 75%; and (iii) the next voting cannot be earlier than two months.

1. QCousins
    1. QLatvia
    * QTurkey
    * QHungary
    * QBalkan
    * QPoland
* QKitchen
* QWomen&
* QJunior
* QUniversity
* QMentor Training

The coordinators of all channels are listed on [the contact page of QWorld](http://qworld.lu.lv/index.php/contact/).

Once the initialization phase ends, the new coordinators of the channel are elected by voting among the leading members of the channel. The rules are defined in each channel separately. The rules of QWorld is followed, if it is not defined.

## Regulations

Each member of QWorld is responsible to check **each new version of / the first**

* code of ethics and
* code of conduct,

and then, the member must agree to read, accept, and follow **the new / the first** version

The same person can be a coordinator of QWorld for **at most two years**. If there is not enough eligible candidates, the term spent during the initialization phase may not be counted. Both rules are applied to each channel listed above.&nbsp;

During the initialization phase, QWorld or each channel must have at least one coordinator. After the initialization phase ends, QWorld or each channel listed above must have **at least two** coordinators. A coordinator position may be associated with a specific duty, e.g. *coordinator (budget)* or *coordinator (public relation)*.

Each channel can define further regulations without contradicting with the ones defined here. The cooperation of channels is desirable but it is not mandatory. *The coordinators of QWorld are expected to create a friendly and collaborative environment and formulate good projects for cooperation.*

## Joining QWorld

### Application for a new member

Each new member is automatically accepted after taking and operating a duty in an event or a project of a channel or under a channel. The leader of that event or project or the coordinator of the channel is responsible to keep track of the corresponding new members.

### Application for a project or an event

Each candidate project is submitted to a relevant channel or some channels. The coordinator(s) of the channel(s) is (are) responsible to process the application.

The proposed project can also be submitted to the coordinators of QWorld, who can help in creating a contact with the appropriate channel(s).

### Application for a new channel

Each application must have at least one coordinator, who can take the title of integration member if the channel is accepted to QWorld.

The team of the candidate channel must accept the latest code of ethics and code of conduct.

The application should have enough descriptions about the aim, the target, the proposed projects and events, and the regulations about the governance.

The new application is received and processed by the board of QWorld. The board may ask certain revisions before voting. Once the candidate application is voted:

* If at least 75% of the board members are positive and no more than 10% of the board members are negative, then the new channel is accepted and its initialization phase starts.
* Otherwise, the application is returned back.

## Modifications on "Governance of QWorld"

Any modification or update (except typo, insignificant corrections, adding/deleting/modifying links, etc.) on this document is done by QBoard.

## Previous versions of "Governance of QWorld"

Each previously used version should be listed here and publicly accessible.
