*You can use #general channel under QWorld's slack workspace (https://qworldworkspace.slack.com/) to ask your questions*

1) Install Anaconda

https://www.anaconda.com/distribution/

2) Run "Jupyter notebook" from the start menu or execute command `jupyter notebook` on the terminal. If Jupyter is not installed, install it via `conda` or by executing `conda install jupyter`

The home page for the notebook will be opened on your browser, which is also called the dashboard.

On the dashboard, the notebooks and files in the current directory are displayed, the running notebooks can be seen and shut down, a new terminal can be opened, etc.

3) Download or clone our tutorial from

https://gitlab.com/qkitchen/qworld-documentation/-/tree/release/templates

Extract/clone our tutorial under a directory accessible from the dashboard, and then go to the root directory of our tutorial from the dashboard.

If Jupyter notebook can be lunched from the terminal, then you can also execute command `jupyter notebook` from the terminal when in the root directory of our tutorial.

4) Open *index.ipynb* from the dashboard by clicking on it.
